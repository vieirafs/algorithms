#include <stdio.h>

// print all array
void print_arr(int arr[], int size) 
{
  int i;

  for (i = 0; i < size; i++) 
  {
    printf("%d ", arr[i]);
  }
  printf("\n");
}

void bubble_sort(int arr[], int size) 
{
  int i, j, temp;
  // position on array
  for (i = 0; i < size; i++) 
  {
    // compare others numbers
    for (j = 0; j < size - i; j++) 
    {
      // if bigger than swhitch
      if (arr[j] > arr[j + 1]) 
      {
        temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }

  print_arr(arr, size);
}

int main()
{

  int arr[] = {2,4,3,6,9};
  int size = sizeof(arr)/sizeof(int);

  printf("size = %d\n", size);
  print_arr(arr,size);

  bubble_sort(arr, size);

  return 0;
}
